**PROVA RENNER**

Projeto destinado ao desafio técnico de um processo seletivo.

Neste projeto foram utilizados as seguintes Tecnologias:

* Java 11
* Maven
* Rest Assured
* Junit 5
* Lombok

---

Para execução do projeto, pasta está dentro da pasta raiz **prova_renner_api** e executar o seguinte comando:

`mvn clean test`

`mvn` comando maven, com o `clean` para limpar(excluir) a pasta target, `test` para execução dos testes


Observação: No projeto foi criado a suite: **TestSuiteReqRes** para que seja executado todas as classes
de testes separadas.
