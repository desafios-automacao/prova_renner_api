package br.com.renner.reqres.model;

import br.com.renner.reqres.core.BaseTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static br.com.renner.reqres.core.Constantes.*;
import static io.restassured.RestAssured.*;

public class DeleteModel extends BaseTest {

    public DeleteModel () {

    }

    protected Response deleteUser() {
        return given()
                .when()
                .contentType(ContentType.JSON)
                .delete(PATH_UP_DEL)
                .then()
                .extract()
                .response()
                .prettyPeek();
    }
}
