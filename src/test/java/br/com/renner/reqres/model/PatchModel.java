package br.com.renner.reqres.model;

import br.com.renner.reqres.core.BaseTest;
import br.com.renner.reqres.utils.DadosUtil;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONObject;

import static br.com.renner.reqres.core.Constantes.*;
import static io.restassured.RestAssured.*;

public class PatchModel extends BaseTest {

    JSONObject usuarioObj = new JSONObject();

    JSONObject atualizaComPatch() {
        usuarioObj.append("name", "Candidato Número: " + DadosUtil.aleatorio(20));
        usuarioObj.accumulate("job", "QA Specialist");
        return usuarioObj;
    }

    protected Response updateUserPatch () {
        return given()
                .when()
                .body(atualizaComPatch().toString())
                .contentType(ContentType.JSON)
                .patch(PATH_UP_DEL)
                .then()
                .extract()
                .response()
                .prettyPeek();
    }
}
