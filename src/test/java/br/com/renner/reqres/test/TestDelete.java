package br.com.renner.reqres.test;

import br.com.renner.reqres.model.DeleteModel;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestDelete extends DeleteModel {

    /* DELETA UM USUARIO */
    @Test
    void testDeleteUserStatus204() {
        Response response = deleteUser();
        assertEquals(HttpStatus.SC_NO_CONTENT, response.statusCode());
        System.out.println("Teste DELETE Realizado com Sucesso! ");
    }
}
