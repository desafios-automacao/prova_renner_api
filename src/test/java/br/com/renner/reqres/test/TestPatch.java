package br.com.renner.reqres.test;

import br.com.renner.reqres.model.PatchModel;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPatch extends PatchModel {

    /* ATUALIZA UM USUARIO COM PATCH */
    @Test
    void testUpdateUserPatchStatus200() {
        Response response = updateUserPatch();
        assertEquals(HttpStatus.SC_OK, response.statusCode());
    }
}
